import { Column, Entity, PrimaryGeneratedColumn, ManyToOne, OneToMany } from 'typeorm';

import { User } from './User';
import { Card } from './Card';

@Entity()
export class Deck {
  @PrimaryGeneratedColumn('uuid')
  public id?: string;

  @Column()
  public name?: string;

  @ManyToOne((type) => User, (user) => user.decks)
  public owner?: User;

  @OneToMany((type) => Card, (card) => card.deck, { onDelete: 'CASCADE' })
  public cards?: Card;
}
