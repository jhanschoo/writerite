import { Column, Entity, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';

import { Deck } from './Deck';

@Entity()
export class Card {
  @PrimaryGeneratedColumn('uuid')
  public id?: string;

  @Column()
  public front?: string;

  @Column()
  public back?: string;

  @ManyToOne((type) => Deck, (deck) => deck.cards)
  public deck?: Deck;
}
