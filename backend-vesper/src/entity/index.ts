import { Card } from './Card';
import { Deck } from './Deck';
import { User } from './User';

export const entities = [
  Card,
  Deck,
  User,
];
