import { Column, Entity, PrimaryGeneratedColumn, ManyToOne, OneToMany, ManyToMany } from 'typeorm';

import { User } from './User';
import { Deck } from './Deck';
import { RoomMessage } from './RoomMessage';

@Entity()
export class Room {
  @PrimaryGeneratedColumn('uuid')
  public id?: string;

  @Column()
  public active?: boolean;

  @ManyToOne((type) => User, (user) => user.rooms)
  public owner?: User;

  @ManyToMany((type) => User, (user) => user.roomOccupantOf)
  public occupants?: User[];

  @OneToMany((type) => RoomMessage, (message) => message.room)
  public messages?: RoomMessage[];

  // TODO: add deck metadata
}
