import { Column, Entity, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';

import { RoomMessage } from './RoomMessage';
import { User } from './User';

@Entity()
export class UserRoomMessage extends RoomMessage {
  @ManyToOne((type) => User, (user) => user.sentRoomMessages)
  public sender?: User;

  public get readMessage() {
    const message = super.readMessage;
    message.sender = this.sender;
    return message;
  }
}
