import { Column, Entity, Index, OneToMany, ManyToMany } from 'typeorm';
import { ValidateIf, IsDefined } from 'class-validator';

import { CurrentUser } from '../model/CurrentUser';
import { Deck } from './Deck';
import { Room } from './Room';
import { UserRoomMessage } from './UserRoomMessage';

@Entity()
export class User extends CurrentUser {
  // One of passwordHash, googleId, or facebookId must be present
  @ValidateIf((o) => !o.googleId && !o.facebookId)
  @IsDefined()
  @Column({ nullable: true })
  public passwordHash?: string;

  @Index({ unique: true })
  @Column({ unique: true, nullable: true })
  public googleId?: string;

  @Index({ unique: true })
  @Column({ unique: true, nullable: true })
  public facebookId?: string;

  @OneToMany((type) => Deck, (deck) => deck.owner)
  public decks?: Deck[];

  @OneToMany((type) => Room, (room) => room.owner)
  public rooms?: Room[];

  @OneToMany((type) => UserRoomMessage, (message) => message.sender)
  public sentRoomMessages?: UserRoomMessage[];

  @ManyToMany((type) => Room, (room) => room.occupants)
  public roomOccupantOf?: Room[];
}
