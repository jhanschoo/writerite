import config from 'config';
import { EntityManager } from 'typeorm';
import { Service } from 'typedi';
import fetch from 'node-fetch';

import { User } from '../entity/User';
import { AbstractAuthService } from './AbstractAuthService';

const AUTH: any = config.get('auth');
let FB_ACCESS_TOKEN = 'FB_ACCESS_TOKEN not set!';

const FB_ACCESS_TOKEN_QUERY = `https://graph.facebook.com/oauth/access_token?client_id=${
  AUTH.facebook_app_id
}&client_secret=${
  AUTH.facebook_app_secret
}&grant_type=client_credentials`;

fetch(FB_ACCESS_TOKEN_QUERY).then((response) => {
  response.json().then((json) => {
    FB_ACCESS_TOKEN = json.access_token;
  });
});

@Service()
export class FacebookAuthService extends AbstractAuthService {
  constructor(
    private entityManager: EntityManager,
  ) {
    super();
  }

  public async signin(email: string, token: string, identifier: string, persist?: boolean) {
    const facebookId = await this.verify(token);
    if (!facebookId || facebookId !== identifier) {
      return null;
    }
    const knownUser = await this.entityManager.findOne(User, { email, facebookId });
    if (knownUser) {
      return FacebookAuthService.authResponseFromUser(knownUser as User, persist);
    }
    const roles = ['user'];
    const user = await this.entityManager.save(User, { email, facebookId, roles });
    return FacebookAuthService.authResponseFromUser(user, persist);
  }

  protected async verify(token: string) {
    const VERIFY_URL = `https://graph.facebook.com/debug_token?input_token=${token
    }&access_token=${FB_ACCESS_TOKEN}`;

    return fetch(VERIFY_URL).then((response) => {
      return response.json().then((json) => {
        if (json.data.app_id === AUTH.facebook_app_id && json.data.is_valid) {
          return json.data.user_id as string;
        }
      });
    });
  }
}
