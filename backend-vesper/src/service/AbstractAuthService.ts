import config from 'config';

import { User } from '../entity/User';
import { generateJWT } from '../util';
import { Service } from 'typedi';

@Service()
export abstract class AbstractAuthService {
  protected static authResponseFromUser = (user: User, persist: boolean = false) => {
    const { id, email, roles } = user;
    return {
      token: generateJWT({ id, email, roles }, persist),
      user,
    };
  }

  public abstract async signin(email: string, token: string, identifier: string, persist?: boolean): Promise<any>;
  protected abstract async verify(token: string): Promise<any>;
}
