import config from 'config';
import { EntityManager } from 'typeorm';
import { Service } from 'typedi';
import { OAuth2Client } from 'google-auth-library';

import { User } from '../entity/User';
import { AbstractAuthService } from './AbstractAuthService';

const AUTH: any = config.get('auth');

const googleClient = new OAuth2Client(AUTH.google_client_id);

@Service()
export class GoogleAuthService extends AbstractAuthService {
  constructor(
    private entityManager: EntityManager,
  ) {
    super();
  }

  public async signin(email: string, token: string, identifier: string, persist?: boolean) {
    const googleId = await this.verify(token);
    if (!googleId || googleId !== identifier) {
      return null;
    }
    const knownUser = await this.entityManager.findOne(User, { email, googleId });
    if (knownUser) {
      return GoogleAuthService.authResponseFromUser(knownUser, persist);
    }
    const roles = ['user'];
    const user: User = await this.entityManager.save(User, { email, googleId, roles });
    return GoogleAuthService.authResponseFromUser(user, persist);
  }

  protected async verify(idToken: string) {
    return googleClient.verifyIdToken({
      audience: AUTH.google_client_id,
      idToken,
    }).then((ticket) => {
      if (!ticket || ticket === null) {
        return;
      }
      const payload = ticket.getPayload();
      if (!payload) {
        return;
      }
      return payload.sub;
    });
  }
}
