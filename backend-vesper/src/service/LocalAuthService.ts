import config from 'config';
import { EntityManager } from 'typeorm';
import fetch from 'node-fetch';
import FormData from 'form-data';
import { Service } from 'typedi';

import { AbstractAuthService } from './AbstractAuthService';
import { User } from '../entity/User';
import { comparePassword, hashPassword } from '../util';

const AUTH: any = config.get('auth');

@Service()
export class LocalAuthService extends AbstractAuthService {
  constructor(
    private entityManager: EntityManager,
  ) {
    super();
  }

  public async signin(email: string, token: string, password: string, persist?: boolean) {
    const knownUser = await this.entityManager.findOne(User, { email });
    if (knownUser) {
      if (knownUser.passwordHash
        && await comparePassword(password, knownUser.passwordHash)) {
          return LocalAuthService.authResponseFromUser(knownUser, persist);
      }
      return null;
    }
    const verified = await this.verify(token);
    if (!verified) {
      return null;
    }
    // create
    const passwordHash = await hashPassword(password);
    const roles = ['user'];
    const user: User = await this.entityManager.save(User, { email, passwordHash, roles });
    return LocalAuthService.authResponseFromUser(user, persist);
  }

  protected async verify(token: string) {
    const form = new FormData();
    form.append('secret', AUTH.recaptcha_secret);
    form.append('response', token);

    return fetch('https://www.google.com/recaptcha/api/siteverify', {
      method: 'post',
      body: form,
    }).then((response) => {
      // TODO: assert that hostname is correct
     return response.json().then((json) => json.success);
    });
  }
}
