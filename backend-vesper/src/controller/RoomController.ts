import { Controller, Query, Mutation, Subscription } from 'vesper';
import { EntityManager } from 'typeorm';
import { PubSub } from 'graphql-subscriptions';

import { CurrentUser } from '../model/CurrentUser';
import { Room } from '../entity/Room';
import { RoomMessage } from '../entity/RoomMessage';

@Controller()
export class RoomController {
  constructor(
    private entityManager: EntityManager,
    private currentUser: CurrentUser,
    private pubSub: PubSub,
  ) {}

  // room(id: ID!): Room
  @Query()
  public async room(id: string): Promise<Room | undefined | null> {
    return this.entityManager.findOne(Room, id);
  }

  // roomCreate(owner: ID!): Room
  @Mutation()
  public async roomCreate(): Promise<Room | undefined | null> {
    const room = this.entityManager.create(Room, {
      owner: { id: this.currentUser.id },
      occupants: [{ id: this.currentUser.id }],
    });
    return this.entityManager.save(Room, room);
  }

  // roomAddOccupant(room: ID! occupant: ID!): Room
  @Mutation()
  public async roomAddOccupant(room: string, occupant: string): Promise<Room | undefined | null> {
    const roomObj = await this.entityManager.findOne(Room, room, {
      relations: ['occupants'],
    });
    if (!roomObj || !roomObj.occupants) {
      return null;
    }
    if (!(occupant in roomObj.occupants.map((user) => user.id))) {
      roomObj.occupants.push({ id: occupant });
      return this.entityManager.save(roomObj);
    }
    return null;
  }

  // roomAddMessage(room: ID! messageContent: String!): Room
  @Mutation()
  public async roomAddMessage(room: string, messageContent: string): Promise<Room | undefined | null> {
    const roomObj = await this.entityManager.findOne(Room, room);
    if (!roomObj || roomObj.owner !== this.currentUser.id) {
      return null;
    }
    const message = this.entityManager.create(RoomMessage, {
      room: { id: room },
      content: messageContent,
    });
    const messageObj = await this.entityManager.save(RoomMessage, message);
    if (!messageObj || !messageObj.room) {
      return null;
    }
    this.pubSub.publish('newRoomMessage', { message: messageObj.room.id });
    return messageObj;
  }

  // newRoomMessage(room: ID!): RoomMessage!
  @Subscription()
  public newRoomMessage({ message }: { message: RoomMessage }, { subscrRoom }: { subscrRoom: string }): boolean {
    return (!!message.room) && message.room.id === subscrRoom;
  }
}
