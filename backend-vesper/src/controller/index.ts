import { AuthController } from './AuthController';
import { CardController } from './CardController';
import { DeckController } from './DeckController';
import { UserController } from './UserController';
import { RoomController } from './RoomController';

export const controllers = [
  AuthController,
  CardController,
  DeckController,
  UserController,
  RoomController,
];
