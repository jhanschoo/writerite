import { Authorized, Controller, Query, Mutation } from 'vesper';
import { EntityManager } from 'typeorm';
import { Deck } from '../entity/Deck';
import { Card } from '../entity/Card';
import { CurrentUser } from '../model/CurrentUser';

@Controller()
export class CardController {
  constructor(
    private entityManager: EntityManager,
    private currentUser: CurrentUser) {
  }

  // card(id: String): Card
  @Query()
  public card({ id }: { id: string }) {
    return this.entityManager.findOne(Card, id);
  }

  // cardsFromDeck(id: String): [Card!]
  @Query()
  public async cardsFromDeck({ id }: { id: string }) {
    return this.entityManager.find(Card, {
      relations: ['deck'],
      where: {
        deck: id,
      },
    });
  }

  // cardSave(id: ID front: String! back:String! deck: ID!): Card
  @Authorized()
  @Mutation()
  public async cardSave({ id, front, back, deck }:
    { id?: string, front: string, back: string, deck: string }) {
    const deckObj = await this.entityManager.findOne(Deck, deck);
    if (!deckObj || deckObj.owner !== this.currentUser.id) {
      return null;
    } else {
      if (id) {
        const card = await this.entityManager.findOne(Card, id);
        if (card && card.deck === deckObj.id) {
          card.front = front;
          card.back = back;
          return this.entityManager.save(Card, card);
        }
        return null;
      } else {
        const card = this.entityManager.create(Card, { front, back, deck: { id: deck } });
        return this.entityManager.save(Card, card);
      }
    }
  }

  // cardDelete(id: ID!): Boolean!
  @Authorized()
  @Mutation()
  public async cardDelete(deck: { id: string }) {
    await this.entityManager.remove(Deck, deck);
    return true;
  }
}
