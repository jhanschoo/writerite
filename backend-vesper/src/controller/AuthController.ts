import { Controller, Mutation } from 'vesper';
import { GoogleAuthService } from '../service/GoogleAuthService';
import { FacebookAuthService } from '../service/FacebookAuthService';
import { LocalAuthService } from '../service/LocalAuthService';

@Controller()
export class AuthController {
  constructor(
    private googleAuth: GoogleAuthService,
    private facebookAuth: FacebookAuthService,
    private localAuth: LocalAuthService,
  ) {}

  /* signin(
   * email: String!
   * token: String!
   * authorizer: String!
   * identifier: String!
   * persist: Boolean
   * ): AuthResponse
   */
  @Mutation()
  public async signin({ email, token, authorizer, identifier, persist }: {
    email: string, token: string, authorizer: string, identifier: string, persist?: boolean,
  }) {
    if (authorizer === 'LOCAL') {
      return this.localAuth.signin(email, token, identifier, persist);
    }
    if (authorizer === 'GOOGLE') {
      return this.googleAuth.signin(email, token, identifier, persist);
    }
    if (authorizer === 'FACEBOOK') {
      return this.facebookAuth.signin(email, token, identifier, persist);
    }
    return null;
  }
}
