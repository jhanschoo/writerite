import { PubSub } from 'graphql-subscriptions';
import { Action, bootstrap } from 'vesper';
import helmet from 'helmet';

import { controllers } from './controller';
import { entities } from './entity';
import { CurrentUser } from './model/CurrentUser';

import { getClaims } from './util';


const pubSub = new PubSub();

const schemas = [
  `${__dirname}/schema/**/*.graphql`,
];

const authorizationChecker = async (roles: string[], action: Action) => {
  const sub = getClaims(action);
  if (sub) {
    const roleClaims = new Set<string>(sub.roles);
    if (roles.filter((role) => roleClaims.has(role))) {
      return true;
    }
  }
  throw new Error('You do not have the appropriate permission.');
};

bootstrap({
  authorizationChecker,
  controllers,
  cors: {
    origin: [/localhost/, /https:\/\/writerite.site/],
    credentials: true,
  },
  entities,
  port: 3000,
  schemas,
  setupContainer: async (container, action) => {
    const sub = getClaims(action);
    const currentUser = new CurrentUser(sub);
    container.set(CurrentUser, currentUser);
    container.set(PubSub, pubSub);
  },
  subscriptionAsyncIterator: (triggers) => pubSub.asyncIterator(triggers),
  use: [helmet()],
}).then(() => {
  // tslint:disable-next-line:no-console
  console.log('Your app is up and running on http://localhost:3000. ' +
  'You can use playground in development mode on http://localhost:3000/playground');
}).catch((error) => {
  // tslint:disable-next-line:no-console
  console.error(error.stack ? error.stack : error);
});
